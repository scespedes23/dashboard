export const LANDING = '/';
export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const HOME = '/home';
export const ACCOUNT = '/account';
export const ADMIN = '/admin';
export const PASSWORD_FORGET = '/pw-forget';
export const V1 = '/v1';
export const V2 = '/v2';
export const V3 = '/v3';
export const V4 = '/v4';
export const V5 = '/v5';
export const V6 = '/v6';
export const V7 = '/v7';
export const V8 = '/v8';
